# Custom Ubuntu kernel with secure boot

Learning how to build a customized Ubuntu kernel, that will run and validated without disabling UEFI Secure Boot.

## Create signing keys

```
openssl req -config custom-kernel-signing-key.openssl.conf \
    -new -x509 -newkey rsa:2048 \
    -nodes -days 36500 -outform DER \
    -keyout "custom-kernel-signing-key.priv" \
    -out "custom-kernel-signing-key.der"
```

## References

* [How to sign things for Secure Boot | Ubuntu](https://ubuntu.com/blog/how-to-sign-things-for-secure-boot)
